import {inject} from 'aurelia-dependency-injection';
import {HttpClient} from 'aurelia-fetch-client';
import {DialogService} from 'aurelia-dialog';
import numeral from 'numeral';

@inject(HttpClient, DialogService)
export class Predictor {

  bedrooms = null;
  bathrooms = null;
  sqft_living = null;
  sqft_lot = null;
  floors = null;
  zipcode = null;
  price = 0;

  constructor(http, dialogService) {
    http.configure(config => {
      config
        .useStandardConfiguration()
        .withBaseUrl('http://localhost:8000/')
    })
    this.http = http;
    this.dialogService = dialogService;
  }

  predict() {
    var formData = this.formToJson();
    console.log(formData);
    return this.sendToPredictor(formData);
  }

  sendToPredictor(formData) {
    return this.http.fetch('predictor', {
      method: 'POST',
      body: formData,
      headers: {'Content-Type': 'application/json'},
      mode: 'cors'
    }).then(response => response.json())
      .then(price => this.price = price)
      .then(reason => {console.log(reason)});
  }

  formToJson() {
    return JSON.stringify(this.getFormData());
  }

  getFormData() {
    var formData = {'bedrooms': [this.bedrooms],
                'bathrooms': [this.bathrooms],
                'sqft_living': [this.sqft_living],
                'sqft_lot': [this.sqft_lot],
                'floors': [this.floors] };
    return formData;
  }
};

export class ShowPriceValueConverter {
  toView(value, format) {
    if (value < 0) value = 0;

    return numeral(value).format(format);
  }
}
