export class App {
  configureRouter(config, router) {
    config.title = 'Aurelia';
    config.map([
      { route: '', name: 'house-price-predictor', moduleId: 'predictor', nav: true, title: 'House Price Predictor'}
    ]);

    this.router = router;
  }
}
